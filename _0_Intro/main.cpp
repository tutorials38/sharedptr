
//=========================================================

class TestClassBase { /* ... */ };

//---------------------------------------------------------

class TestClass : public TestClassBase
{
public:
    TestClass() {}
    TestClass(int) {}

    void method() { }

private:
};

//=========================================================

int main()
{
    //---------------[1]---------------{ Constructing, access & free on destructing }
    {
        TestClass* thePointer = new TestClass();//    <<<--- We should be able to construct object

        //        VV
        thePointer->method();//                       <<<--- It's should be possible to access object members
        //        ^^
    }//                                               <<<--- Memory should be free here

    //---------------[2]---------------{ Free on assigning }
    {
        TestClass* thePointer = new TestClass();

        //         V
        thePointer = new TestClass();//               <<<--- Free first created object memory here
        //         ^

        //         V
        thePointer = nullptr;//                       <<<--- Free second created object memory here
        //         ^
    }

    //---------------[3]---------------{ Free only after no pointers left }
    {
        TestClass* thePointer = new TestClass();
        TestClass* theOtherPointer = thePointer;

        //         V
        thePointer = nullptr;//                       <<<--- Free will be performed not here...
        //         ^

        //              V
        theOtherPointer = nullptr;//                  <<<--- ...but only here
        //              ^
    }

    //---------------[4]---------------{ Support of all constructors }
    {
        //                                    VV
        TestClass* thePointer = new TestClass(42);//  <<<--- Call any supported constructor
        //                                    ^^
    }

    //---------------[5]---------------{ Base class assigning }
    {
        TestClass* thePointer = new TestClass();

        //VVVVVVVVVVVV
        TestClassBase* theBasePointer = thePointer;// <<<--- Assigning of parent pointer should be supported too
        //^^^^^^^^^^^^
    }

    return 0;
}

//RAII - Resource Acquisition Is Initialization
