#include <iostream>
#include <memory>
#include <utility>

//========================== Shared Pointer

//-------------- Block

struct SharedBlockBase
{
    unsigned int referencingPointersNum = 0;
};

template<typename T>
struct SharedBlock : public SharedBlockBase
{
    template<typename ... ConstructorArgs>
    SharedBlock(ConstructorArgs&& ... inArgs)
        : SharedBlockBase(), value(std::forward<ConstructorArgs>(inArgs) ...) { }

    T value;
};

//-------------- Pointer

template<typename T>
class SharedPointer
{
public:
    //[1] --- Default construction
    SharedPointer()
        : SharedPointer(nullptr, nullptr) { }

    //[2] --- Copy construction
    template<typename OtherT>
    SharedPointer(const SharedPointer<OtherT>& inOther)
        : SharedPointer(static_cast<T*>(inOther._pointer), inOther._sharedBlock) { }

    SharedPointer(const SharedPointer<T>& inOther)
        : SharedPointer(inOther._pointer, inOther._sharedBlock) { }

    //[3] --- Copy
    void reset() {
        printState("reset start {");
        release();
        resetPointers();
        printState("reset end }");
    }

    template<typename OtherT>
    SharedPointer& operator=(const SharedPointer<OtherT>& inOther) {
        return assign(static_cast<T*>(inOther._pointer), inOther._sharedBlock);
    }

    SharedPointer& operator=(const SharedPointer<T>& inOther) {
        return assign(inOther._pointer, inOther._sharedBlock);
    }

    //[4] --- Access
    T* operator->() const {
        return _pointer;
    }

    T& operator*() const {
        return *_pointer;
    }

    //[5] --- Comparation
    template<typename OtherT>
    bool operator ==(const SharedPointer<OtherT>& inOther) const {
        return _pointer == inOther._pointer;
    }

    bool operator ==(const SharedPointer<T>& inOther) const {
        return _pointer == inOther._pointer;
    }

    operator bool() const {
        return _pointer;
    }

    //[6] --- Destroy
    ~SharedPointer() {
        printState("destructor start {");
        release();
        printState("destructor end }");
    }

private:
    template<typename Type, typename ... ConstructorArgs>
    friend SharedPointer<Type> makeShared(ConstructorArgs&& ... inArgs);

    template<typename ToType, typename FromType>
    friend SharedPointer<ToType> sharedDynamicCast(const SharedPointer<FromType>& inPointer);

    SharedPointer(T* inPointer, SharedBlockBase* inNewSharedBlock)
        : _pointer(inPointer), _sharedBlock(inNewSharedBlock)
    {
        printState("constructor start {");
        retain();
        printState("constructor end }");
    }

    SharedPointer<T>& assign(T* inPointer, SharedBlockBase* inSharedBlock) {
        printState("assignmen start (other type) {");
        release();
        _pointer = inPointer;
        _sharedBlock = inSharedBlock;
        retain();
        printState("assignmen end (other type) }");
        return this;
    }

    void retain() {
        printState("retain start {");
        if (_sharedBlock) {
            ++_sharedBlock->referencingPointersNum;
        }
        printState("retain end }");
    }

    void release() {
        printState("release start {");
        if (_sharedBlock) {
            if (--_sharedBlock->referencingPointersNum == 0) {
                delete _sharedBlock;
                resetPointers();
                printState("[!!!BLOCK DESTRUCTED!!!]");
            }
        }
        printState("release end }");
    }

    void resetPointers() {
        _pointer = nullptr;
        _sharedBlock = nullptr;
    }

    void printState(const char* inAdditionalMessage) {
        std::cout << "[SharedPointer| this: " << this << " | ";
        if (_pointer) {
            std::cout <<
                "pointer: " << _pointer << " | "
                "block refs num: " << _sharedBlock->referencingPointersNum;
        } else {
            std::cout << "EMPTY";
        }
        std::cout << "]: " << inAdditionalMessage << std::endl;
    }

    T* _pointer = nullptr;
    SharedBlockBase* _sharedBlock = nullptr;
};

//-------------- Make shared

template<typename Type, typename ... ConstructorArgs>
SharedPointer<Type> makeShared(ConstructorArgs&& ... inArgs) {
    auto* theSharedBlock = new SharedBlock<Type>(std::forward<ConstructorArgs>(inArgs) ...);
    return { &theSharedBlock->value, theSharedBlock };
}

//-------------- Dynamic cast shared

template<typename ToType, typename FromType>
SharedPointer<ToType> sharedDynamicCast(const SharedPointer<FromType>& inPointer)
{
    auto* theCastedPointer = dynamic_cast<ToType*>(inPointer._pointer);
    SharedBlockBase* theSharedBlock = theCastedPointer ? inPointer._sharedBlock : nullptr;
    return { theCastedPointer, theSharedBlock };
}

//========================== Test

int main()
{
    std::cout << " --------------------------------------{1} " << std::endl;
    auto thePointerA = makeShared<int>();
    std::cout << " --------------------------------------{2} " << std::endl;
    SharedPointer<int> thePointerB = thePointerA;
    std::cout << " --------------------------------------{3} " << std::endl;

    return 0;
}
