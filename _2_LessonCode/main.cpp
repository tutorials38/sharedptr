
// ----------------------------

class Tank
{
public:

    Tank(int inX, int inY, int inHitPoints)
        : x(inX), y(inY), hitPoints(inHitPoints) { }

    void update() {
        x += 1;
    }

    bool applyDamange(int inDamage) {
        hitPoints -= inDamage;
        return (hitPoints <= 0);
    }


    int getX() {
        return x;
    }

    int getY() {
        return y;
    }

private:
    int x;
    int y;
    int hitPoints;
};

// ----------------------------

struct ControlBlock
{
    int refCount = 0;
    Tank* rawPointer = nullptr;
};

class TankSharedPointer
{
public:
    TankSharedPointer(ControlBlock* inControlBlockPointer)
        : controlBlockPointer(inControlBlockPointer)
    {
        retainBlock();
    }

    TankSharedPointer(const TankSharedPointer& inOtherPointer)
        : controlBlockPointer(inOtherPointer.controlBlockPointer)
    {
        retainBlock();
    }

    void set(TankSharedPointer inRawPointer) {
        releaseBlock();

        controlBlockPointer = inRawPointer.controlBlockPointer;
        
        retainBlock();
    }

    Tank* get() {
        return controlBlockPointer->rawPointer;
    }

    bool isValid() {
        return (controlBlockPointer != nullptr);
    }

    ~TankSharedPointer() {
        releaseBlock();
    }

private:
    void retainBlock() {
        ++controlBlockPointer->refCount;
    }

    void releaseBlock() {
        if (controlBlockPointer)
        {
            --controlBlockPointer->refCount;
            if (controlBlockPointer->refCount == 0)
            {
                delete controlBlockPointer->rawPointer;
                delete controlBlockPointer;
            }
        }
    }


    ControlBlock* controlBlockPointer = nullptr;
};

// ----------------------------

class Rocket
{
public:
    Rocket(TankSharedPointer inTankPointer)
        : tankPointer(inTankPointer) { }

    void update() {
        //persuit(tankPointer->getX(), tankPointer->getY());
    }

private:
    void persuit(int x, int y) {
        //some logic
    }

    TankSharedPointer tankPointer;
};

// ----------------------------

//
//
//
//       T
// Y
// ^
// |_> X

// ----------------------------

int main()
{
    ControlBlock* theControlBlock = new ControlBlock();
    theControlBlock->rawPointer = new Tank(2, 2, 100);

    TankSharedPointer theTankPointer = TankSharedPointer(theControlBlock);
    TankSharedPointer theOtherTankPointer = TankSharedPointer(theControlBlock);

    //SharedPointer<Rocket> theRocketA = MakeShared<Rocket>(theTank);
    //SharedPointer<Rocket> theRocketB = MakeShared<Rocket>(theTank);

    int lastGameFrameToMake = 10;
    int currentFrame = 0;

    while (currentFrame < lastGameFrameToMake)
    {
        theTankPointer.get()->update();
        //theRocketA->update();
        //theRocketB->update();

        // ...

        bool isTankShouldBeKilled = theTankPointer.get()->applyDamange(10);
        if (isTankShouldBeKilled)
        {
            ControlBlock* theControlBlock = new ControlBlock();
            theControlBlock->rawPointer = new Tank(2, 2, 100);

            theTankPointer.set(TankSharedPointer(theControlBlock));
        }

        //delay()

        theOtherTankPointer.get()->applyDamange(10);

        ++currentFrame;
    }

    return 0;
}
